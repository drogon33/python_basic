i = 5
def f(arg=i):
  print(arg)
i = 6
f()

def f2(a, L=[]):
  L.append(a)
  return L

print(f2(1))
print(f2(2))
print(f2(3))

def f3(a, L=None):
  if L is None:
    L = []
  L.append(a)
  return L
