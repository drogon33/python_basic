#uses most of the list methods:
fruits = ['orange', 'apple', 'pear', 'banana', 'kiwi', 'apple', 'banana']
print(fruits.count('apple'))
print(fruits.count('tangerine'))
print(fruits.index('apple'))
fruits.reverse()
print(fruits)
fruits.sort()
print(fruits)
fruits.pop()
print(fruits.pop())

#sing Lists as Stacks
stack = [3, 4, 5]
stack.append(6)
stack.append(7)
print(stack)
print(stack.pop())
print(stack)

#Using Lists as Queues
from collections import deque
queue = deque(["Eric", "John", "Michael"])
queue.append("Terry")
print(queue)
queue.popleft()   

#List Comprehensions
squares = []
for x in range(10):
  squares.append(x**2)
print(squares)
#same
squares = list(map(lambda x: x**2, range(10)))
print(squares)
#same
squares = [x**2 for x in range(10)]
print(squares)

list=[(x, y) for x in [1,2,3] for y in [3,1,4] if x != y]
print(list)
#same
combs = []
for x in [1,2,3]:
  for y in [3,1,4]:
    if x != y:
      combs.append((x, y))
print(combs)

# Nested List Comprehension
matrix = [
  [1, 2, 3, 4],
  [5, 6, 7, 8],
  [9, 10, 11, 12],
]
print(matrix)
#transpose rows and columns:
print([[row[i] for row in matrix] for i in range(4)])
#same previous section
transposed = []
for i in range(4):
  transposed.append([row[i] for row in matrix])
print(transposed)

#same previous section
transposed = []
for i in range(4):
# the following 3 lines implement the nested listcomp
  transposed_row = []
  for row in matrix:
    transposed_row.append(row[i])
  transposed.append(transposed_row)
print(transposed)

#The del statement
a = [-1, 1, 66.25, 333, 333, 1234.5]
del a[0]
print(a)
del a[2:4]
print(a)
del a[:]
print(a)
del a

#Tuples and Sequences
t = 12345, 54321, 'hello!'
print(t[0])
  # Tuples may be nested:
u = t, (1, 2, 3, 4, 5)
print(u)

empty = ()
singleton = 'hello',
print(len(empty))
print(len(singleton))

#tuple packing and reverse
t = 12345, 54321, 'hello!'
x, y, z = t
print(t)

#Sets
# Basic uses include membership testing and eliminating duplicate entries.
basket = {'apple', 'orange', 'apple', 'pear', 'orange', 'banana'}
print(basket)  
print('orange' in basket)
# Demonstrate set operations on unique letters from two words
a = set('abracadabra')
b = set('alacazam')
print(a)
print(a-b)
# letters in a or b or both
print(a|b)
 # letters in both a and b
print(a&b)
 # letters in a or b but not both
print(a^b)
#same
a = {x for x in 'abracadabra' if x not in 'abc'}
print(a)

#Dictionaries
tel = {'jack': 4098, 'sape': 4139}
tel['guido'] = 4127
print(tel)
del tel['sape']
print(tel)
sorted(tel)
print(tel)

#dict() constructor builds dictionaries directly from sequences of key-value pairs:
print(dict([('sape', 4139), ('guido', 4127), ('jack', 4098)]))
dictv1 ={x: x**2 for x in (2, 4, 6)}
print(dictv1)
print(dict(sape=4139, guido=4127, jack=4098))

#Looping Techniques
knights = {'gallahad': 'the pure', 'robin': 'the brave'}
for k, v in knights.items():
  print(k, v)
#show index
for i, v in enumerate(['tic', 'tac', 'toe']):
  print(i, v)

#loop multiple sequences same time
questions = ['name', 'quest', 'favorite color']
answers = ['lancelot', 'the holy grail', 'blue']
for q, a in zip(questions, answers):
  print('What is your {0}?  It is {1}.'.format(q, a))

#loop sequence in reverse
for i in reversed(range(1, 10, 2)):
  print(i)

#loop over a sequence in sorted order,
basket = ['apple', 'orange', 'apple', 'pear', 'orange', 'banana']
for f in sorted(set(basket)):
  print(f)

#loop list in new list
import math
raw_data = [56.2, float('NaN'), 51.7, 55.3, 52.5, float('NaN'), 47.8]
filtered_data = []
for value in raw_data:
  if not math.isnan(value):
    filtered_data.append(value)
print(filtered_data)

#More on Conditions
string1, string2, string3 = '', 'Trondheim', 'Hammer Dance'
non_null = string1 or string2 or string3
print(non_null)

#Comparing Sequences and Other Types
(1, 2, 3)              < (1, 2, 4)
[1, 2, 3]              < [1, 2, 4]
'ABC' < 'C' < 'Pascal' < 'Python'
(1, 2, 3, 4)           < (1, 2, 4)
(1, 2)                 < (1, 2, -1)
(1, 2, 3)             == (1.0, 2.0, 3.0)
(1, 2, ('aa', 'ab'))   < (1, 2, ('abc', 'a'), 4)