def fib(n): 
  a, b = 0, 1
  while a < n:
    print(a, end=' ')
    a, b = b, a+b
    print()

print(fib(100))

#returns a list of the numbers of the Fibonacci series
def fib2(n):
  result = []
  a,b = 0, 1
  while a<n:
    result.append(a)
    a,b = b, a+b
  return result
f100 = fib2(100)
print(f100)

