squares = [1, 4, 9, 16, 25]
print(squares)

#indexs
print(squares[0])

#concatenation in list
print(squares + [36, 49, 64, 81, 100])

#update list
cubes = [1, 8, 27, 65, 125]
cubes[3] = 60
print(cubes)
cubes.append(21)
cubes.append(2**4)
print(cubes)

#replace values
letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
letters[2:5] = ['C', 'D', 'E']
print(letters)
letters[2:5] = []
print(letters)
print(len(letters))
letters[:] = []
print(letters)

x= ['a', 'b', 'c']
y= [1, 2, 3]
m = [x, y]
print(m)
print(m[0])
print(m[0][1])

#fibonacies 
a, b = 0, 1
while a < 10:
  print(a)
  a, b = b, a+b