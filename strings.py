#single quotes
print('sample eggs')
#use \ to escape quotes
print('sample eggs\'t')
#double quotes
print("sample eggs't")
#use \ to escape quotes
print("sample eggs\"t")

print('"Isn\'t," they said.')
s = 'First line.\nSecond line.'
#use \n to break line
print('First line.\nSecond line.')
print(s)

# /n is new line
print('C:\some\name')
# use r before quote to disable \n
print(r'C:\some\name')

#using triple-quotes """...""" or '''...'''
print("""\
Usage: thingy [OPTIONS]
     -h                        Display this usage message
     -H hostname               Hostname to connect to
""")
#use operator for string
print(3 * 'un' + 'ium')
print('Py' 'thon')

#variable in string
prefix = 'Py'
print(prefix + 'thon')
#indexed in strings
word = 'Python'
print(word[0])
print(word[-1])
print(word[0:2])
print(word[:2] + word[2:])

s = 'toan'
print(len(s))