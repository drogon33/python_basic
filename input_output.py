# Fancier Output Formatting¶
#you can write a Python expression between { and } characters
year = 2016
event = 'Referendum'
print(f'Results of the {year} {event}')

#can provide detailed formatting directives
yes_votes = 42_572_654
no_votes = 43_132_495
percentage = yes_votes / (yes_votes + no_votes)
voteOut = '{:-9} YES votes  {:2.2%}'.format(yes_votes, percentage)
print(voteOut)


s = 'Hello, world.'
# function is meant to return representations of values which are fairly human-readable
print(str(s))
#function to generate representations which can be read by the interpreter
print(repr(s))
print(str(1/7))

x = 10 * 3.25
y = 200 * 200
s = 'The value of x is ' + repr(x) + ', and y is ' + repr(y) + '...'
print(s)

# The repr() of a string adds string quotes and backslashes:
hello = 'hello, world\n'
hellos = repr(hello)
print(hellos)

# The argument to repr() may be any Python object:
reorMul=repr((x, y, ('spam', 'eggs')))
print(reorMul)

#Formatted String Literals
import math
print(f'The value of pi is approximately {math.pi:.3f}.')

table = {'Sjoerd': 4127, 'Jack': 4098, 'Dcab': 7678}
for name, phone in table.items():
  print(f'{name:10} ==> {phone:10d}')

#The String format() Method
print('We are the {} who say "{}!"'.format('knights', 'Ni'))

#A number in the brackets can be used to refer to the position of the object passed into the str.format() method.
print('{0} and {1}'.format('spam', 'eggs'))

# use name of argument
print('This {food} is {adjective}.'.format(food='spam', adjective='absolutely horrible'))

#Positional and keyword arguments can be arbitrarily combined:
print('The story of {0}, {1}, and {other}.'.format('Bill', 'Manfred', other='Georg'))
                                                      
#Manual String Formatting
for x in range(1, 11):
  print(repr(x).rjust(2), repr(x*x).rjust(3), end=' ')
  # Note use of 'end' on previous line
  print(repr(x*x*x).rjust(4))

#Old string formatting
import math
print('The value of pi is approximately %5.3f.' % math.pi)

