class MyClass:
  """A simple example class"""
  i = 12345

  def f(self):
    return 'hello world'

print(MyClass.i)

x = MyClass()

def __init__(self):
  self.data = []

# Instance Objects
x.counter = 1
while x.counter < 10:
    x.counter = x.counter * 2
print(x.counter)
del x.counter

#Method Objects
x.f()
xf = x.f
while True:
  print(xf())

