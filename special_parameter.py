#passed by position or keyword
def standard_arg(arg):
  print(arg)
standard_arg(2)

# restricted to only use positional parameters
def pos_only_arg(arg, /):
  print(arg)
pos_only_arg(1)
#pos_only_arg(arg=1)

#only allows keyword arguments as indicated
def kwd_only_arg(*, arg):
  print(arg)
kwd_only_arg(arg=3)
# kwd_only_arg(3)

def combined_example(pos_only, /, standard, *, kwd_only):
  print(pos_only, standard, kwd_only)

# combined_example(1, 2, 3)
combined_example(1, 2, kwd_only=3)
combined_example(1, standard=2, kwd_only=3)
# combined_example(pos_only=1, standard=2, kwd_only=3)

