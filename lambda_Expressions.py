def make_incrementor(n):
  return lambda x: x + n
f = make_incrementor(42)
print(f(0))

pairs = [(1, 'one'), (2, 'two'), (3, 'three'), (4, 'four')]
pairs.sort(key=lambda pair: pair[1])
print(pairs)

def kteam(first_string):
  return lambda second_string: first_string + second_string
slogan = kteam('how kteam')
print(slogan('string1 str2'))